package ex4;


import java.util.Random;

public class TemperatureSensor extends Sensor {
    Random random = new Random();

    public int readValue() {
        return random.nextInt(101);
    }
}