package ex4;

import java.util.Random;

enum Direction {
    UP, DOWN, LEFT, RIGHT;

    public static Direction fromInteger(int x) {
        switch(x) {
            case 0:
                return UP;
            case 1:
                return DOWN;
            case 2:
                return LEFT;
            case 3:
                return RIGHT;
            default:
                return null;
        }
    }
}

public class Robot extends Thread {
    private static final int MAP_SIZE = 10;

    private String name;
    private int x;
    private int y;

    private static Robot[][] map = new Robot[MAP_SIZE][MAP_SIZE];
    private static Random rand = new Random();

    private Robot(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
        System.out.println(this.toString() + " created.");
    }

    public Robot(String name) {
        int i, j;
        while(true) {
            i = rand.nextInt(MAP_SIZE);
            j = rand.nextInt(MAP_SIZE);

            if(map[i][j] == null) {
                map[i][j] = new Robot(name, i, j);
                map[i][j].start();
                break;
            }
        }
    }

    public String toString() {
        return "Robot{" + name + "}(" + x + ", " + y + ")";
    }

    private void eraseFromMap() {
        map[x][y] = null;
    }

    private void moveOnMap() {
        map[x][y] = this;
        System.out.println(this.toString() + " moved.");
    }

    public void run() {
        try {
            while(true) {
                Direction dir = Direction.fromInteger(rand.nextInt(4));
                switch(dir) {
                    case UP:
                        if(this.y > 0) {
                            if (map[x][y - 1] == null) {
                                eraseFromMap();
                                y = y - 1;
                                moveOnMap();
                            }
                            else {
                                System.out.println(this.toString() +
                                        " and " +
                                        map[x][y - 1].toString() +
                                        " destroyed each other.");
                                this.eraseFromMap();
                                map[x][y - 1].eraseFromMap();
                                return;
                            }
                        }
                        break;

                    case DOWN:
                        if(this.y < MAP_SIZE - 1) {
                            if(map[x][y + 1] == null) {
                                eraseFromMap();
                                y = y + 1;
                                moveOnMap();
                            }
                            else {
                                System.out.println(this.toString() +
                                        " and " +
                                        map[x][y + 1].toString() +
                                        " destroyed each other.");
                                this.eraseFromMap();
                                map[x][y + 1].eraseFromMap();
                                return;
                            }
                        }
                        break;

                    case LEFT:
                        if(x > 0) {
                            if(map[x - 1][y] == null) {
                                eraseFromMap();
                                x = x - 1;
                                moveOnMap();
                            }
                            else {
                                System.out.println(this.toString() +
                                        " and " +
                                        map[x - 1][y].toString() +
                                        " destroyed each other.");
                                this.eraseFromMap();
                                map[x - 1][y].eraseFromMap();
                                return;
                            }
                        }
                        break;

                    case RIGHT:
                        if(this.x < MAP_SIZE - 1) {
                            if (map[x + 1][y] == null) {
                                eraseFromMap();
                                x = x + 1;
                                moveOnMap();
                            }
                            else {
                                System.out.println(this.toString() +
                                        " and " +
                                        map[x + 1][y].toString() +
                                        " destroyed each other.");
                                this.eraseFromMap();
                                map[x + 1][y].eraseFromMap();
                                return;
                            }
                        }
                        break;
                }
                Thread.sleep(100);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}
