package ex3;
public class Main {
    public static void main(String[] args) {
        Counter c1 = new Counter("Counter1", 0, 100);
        Counter c2 = new Counter("Counter2", 100, 200);
        c1.start();
        c2.start();
    }
}
