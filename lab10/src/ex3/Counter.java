package ex3;
public class Counter extends Thread {
    private int lower;
    private int upper;

    public Counter(String name, int lower, int upper) {
        super(name);
        this.lower = lower;
        this.upper = upper;
    }

    public void start() {
        if(lower > upper) return;
        for (int i = lower; i <= upper; i++) {
            System.out.println(this.getName() + " i = " + i);
            try {
                Thread.sleep(100);
            }
            catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " job finalised.");
    }
}
