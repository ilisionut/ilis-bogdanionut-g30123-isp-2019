package ex2;

import java.io.*;


public class CharacterCounter {
    public static void main (String args[]) throws IOException{

        char character;
        // 1b. Reading standard input:
        BufferedReader stdin = new BufferedReader(
                new InputStreamReader(System.in));
        System.out.println("Give the character:");
        character=stdin.readLine().charAt(0);

        int number=0;

        //citire din fisier
        BufferedReader in = new BufferedReader(
                new FileReader("C:\\Users\\Ilis Ionut\\ilis-bogdanionut-g30123-isp-2019\\lab7\\src\\ex2\\cuvinte"));
        String s;
        while((s = in.readLine())!= null)
        {
            char[] lin = s.toCharArray();
            for (char c : lin) {
                if (c == character) {
                    number++;
                }
            }
        }
        in.close();
        System.out.println("The character " +character+  " appears " +number+ " times");
    }

}