package ex1;

class CofeeMaker {
    private static int cofeeCount = 0;
    private int LIMIT = 5;

    Cofee makeCofee() throws MakeException {
        if (cofeeCount >= LIMIT) {
            throw (new MakeException("To much cofee made"));
        }
        System.out.println("Make a cofee");
        cofeeCount++;
        int t = (int) (Math.random() * 100);
        int c = (int) (Math.random() * 100);
        return new Cofee(t, c);
    }
}