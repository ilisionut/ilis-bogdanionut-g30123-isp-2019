package ex1;

public class ConcentrationException extends Exception{
    private int c;
    ConcentrationException(int c, String msg) {
        super(msg);
        this.c = c;
    }

    int getConc(){
        return c;
    }
}