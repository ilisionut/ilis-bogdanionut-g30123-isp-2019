package ex4;

public class Test {

    public static void main(String[] args) {
        Dictionary d = new Dictionary();
        Word cuv = new Word("word");
        Definition def = new Definition("description1");
        Word cuv1 = new Word("example");
        Definition def1 = new Definition("description2");
        Word cuv2 = new Word("example2");
        Definition def2 = new Definition("description3");
        d.addWord(cuv,def);
        d.addWord(cuv1,def1);
        d.addWord(cuv2,def2);

        d.getAllDefinition();
        d.getDefinition(cuv);
        d.getAllWords();
    }
}
