package ex3;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class Reader extends JFrame {
    JButton button;
    JLabel label;
    JTextArea input;
    JTextArea output;

    Reader() {
        setTitle("Reader");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(430, 450);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);

        label = new JLabel("Filename");
        label.setBounds(30, 30, 50, 20);

        input = new JTextArea();
        input.setBounds(90, 30, 300, 20);

        button = new JButton("Display");
        button.setBounds(30, 60, 100, 20);
        button.addActionListener(new DisplayButtonPress());

        output = new JTextArea();
        output.setBounds(30, 90, 360, 300);

        add(label);
        add(input);
        add(output);
        add(button);
    }

    class DisplayButtonPress implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            Reader.this.output.setText("");
            try {
                BufferedReader bf = new BufferedReader(
                        new FileReader(
                                new File(
                                        Reader.this.input.getText())));
                String line = "";
                line = bf.readLine();
                while (line != null) {
                    Reader.this.output.append(line + "\n");
                    line = bf.readLine();
                }
            }
            catch (FileNotFoundException fileEx) {
                System.out.println("File not found!");
            }
            catch (IOException IOEx) {
                System.out.println("IO exception!");
            }
        }
    }
}
