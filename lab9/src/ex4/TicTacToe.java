package ex4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TicTacToe extends JPanel {
    JButton[] buttons = new JButton[9];
    int alternate = 0;

    public TicTacToe() {
        setLayout(new GridLayout(3, 3));
        initButtons();
    }

    public void initButtons() {
        for (int i = 0; i < 9; i++) {
            buttons[i] = new JButton();
            buttons[i].setText("");
            buttons[i].addActionListener(new buttonListener());
            add(buttons[i]);
        }
    }

    public void resetButtons() {
        for (int i = 0; i < 9; i++) {
            buttons[i].setText("");
        }
    }

    private class buttonListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            JButton buttonClicked = (JButton) e.getSource();

            if (alternate % 2 == 0)
                buttonClicked.setText("X");
            else
                buttonClicked.setText("O");

            if (isSolution()) {
                String gameStatus = "";

                if(alternate % 2 == 0)
                    gameStatus = "You won!";
                else
                    gameStatus = "You lost!";

                JOptionPane.showConfirmDialog(null, gameStatus, "Game Over!", JOptionPane.DEFAULT_OPTION);
                resetButtons();
            }

            alternate++;
        }

        public boolean isSolution() {
            // horizontal
            if ((check(0, 1) && check(1, 2)) ||
                    (check(3, 4) && check(4, 5)) ||
                    (check(6, 7) && check(7, 8)))
                return true;

                // vertical
            else if ((check(0, 3) && check(3, 6)) ||
                    (check(1, 4) && check(4, 7)) ||
                    (check(2, 5) && check(5, 8)))
                return true;

                // diagonal
            else if ((check(0, 4) && check(4, 8)) ||
                    (check(2, 4) && check(4, 6)))
                return true;

                // not a solution
            else
                return false;
        }

        public boolean check(int a, int b) {
            if (buttons[a].getText().equals(buttons[b].getText()) &&
                    !buttons[a].getText().equals(""))
                return true;
            else
                return false;
        }
    }
}
