package ex2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Counter extends JFrame {
    JButton button;
    JLabel label;
    JTextField field;

    int count;

    Counter() {
        this.count = 0;
        setTitle("Button presses counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(220, 190);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);

        button = new JButton("Increase");
        button.setBounds(30, 30, 150, 50);
        button.addActionListener(new IncrementButtonPress());

        label = new JLabel("Count");
        label.setBounds(30, 100, 50, 20);

        field = new JTextField("0");
        field.setBounds(90, 100, 90, 20);

        this.add(button);
        this.add(label);
        this.add(field);
    }

    class IncrementButtonPress implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            Counter.this.count++;
            Counter.this.field.setText(Integer.toString(Counter.this.count));
        }
    }
}

