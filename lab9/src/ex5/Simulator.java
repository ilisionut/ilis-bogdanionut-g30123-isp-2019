package ex5;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Simulator extends JFrame {
    private JLabel labelTrain, labelStation, labelDestination, labelSegment;
    private JTextField inputTrain, inputStation, inputDestination, inputSegment;
    private static JTextArea displayStation, info;
    private JButton addTrain;

    private static int numberOfTrains;
    private Train[] trains;

    private Controller c1 = new Controller("Cluj-Napoca");
    private Segment s1 = new Segment(1);
    private Segment s2 = new Segment(2);
    private Segment s3 = new Segment(3);

    private Controller c2 = new Controller("Bucuresti");
    private Segment s4 = new Segment(4);
    private Segment s5 = new Segment(5);
    private Segment s6 = new Segment(6);

    private Controller c3 = new Controller("Timisoara");
    private Segment s7 = new Segment(7);
    private Segment s8 = new Segment(8);
    private Segment s9 = new Segment(9);

    public Simulator() {
        setTitle("Train simulator");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        init();
        setSize(1200, 1000);
        setVisible(true);

        numberOfTrains = 0;
        trains = new Train[9];

        c1.addControlledSegment(s1);
        c1.addControlledSegment(s2);
        c1.addControlledSegment(s3);

        c2.addControlledSegment(s4);
        c2.addControlledSegment(s5);
        c2.addControlledSegment(s6);

        c3.addControlledSegment(s7);
        c3.addControlledSegment(s8);
        c3.addControlledSegment(s9);

        c1.addNeighbourController(c2);
        c1.addNeighbourController(c3);

        c2.addNeighbourController(c1);
        c2.addNeighbourController(c3);

        c3.addNeighbourController(c1);
        c3.addNeighbourController(c2);
    }

    private void init() {
        this.setLayout(null);
        int width = 150;
        int height = 40;

        labelTrain = new JLabel("Train");
        labelTrain.setBounds(10, 20, width, height);

        labelStation = new JLabel("Start station");
        labelStation.setBounds(160, 20, width, height);

        labelDestination = new JLabel("Destination");
        labelDestination.setBounds(310, 20, width, height);

        labelSegment = new JLabel("Line");
        labelSegment.setBounds(460, 20, width, height);

        info = new JTextArea("Cluj-Napoca lines: 1, 2, 3\n" +
                "Bucuresti lines: 4, 5, 6\n" +
                "Timisoara lines: 7, 8, 9\n");
        info.setBounds(620, 20, 200, 60);
        info.setEditable(false);

        inputTrain = new JTextField();
        inputTrain.setBounds(10, 65, width, height);

        inputStation = new JTextField();
        inputStation.setBounds(160, 65, width, height);

        inputDestination = new JTextField();
        inputDestination.setBounds(310, 65, width, height);

        inputSegment = new JTextField();
        inputSegment.setBounds(460, 65, width, height);

        addTrain = new JButton("Add train");
        addTrain.setBounds(20, 110, width, height);
        addTrain.addActionListener(new AddButtonPressed());

        displayStation = new JTextArea();
        displayStation.setBounds(20, 160, 800, 600);
        displayStation.setEditable(false);

        add(labelDestination);
        add(labelSegment);
        add(labelStation);
        add(labelTrain);

        add(inputDestination);
        add(inputTrain);
        add(inputSegment);
        add(inputStation);

        add(addTrain);
        add(displayStation);
        add(info);
    }

    class AddButtonPressed implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if(numberOfTrains <= 9) {
                if(displayStation.getText() != null)
                    displayStation.setText(null);

                String stationName = Simulator.this.inputStation.getText();
                String trainName = Simulator.this.inputTrain.getText();
                String destinationName = Simulator.this.inputDestination.getText();

                int segmentId = Integer.parseInt(Simulator.this.inputSegment.getText());

                if(trainName != null) {
                    if(stationName != null) {
                        if(destinationName != null) {
                            if(Simulator.this.inputSegment.getText() != null) {

                                trains[numberOfTrains] = new Train(Simulator.this.inputDestination.getText(),
                                        Simulator.this.inputTrain.getText());

                                if(stationName.equals("Cluj-Napoca") && segmentId >= 1 && segmentId <= 3) {
                                    c1.arriveTrain(trains[numberOfTrains], segmentId);
                                    c1.controlStep();
                                }

                                if (stationName.equals("Bucuresti") && segmentId >= 4 && segmentId <= 6) {
                                    c2.arriveTrain(trains[numberOfTrains], segmentId);
                                    c2.controlStep();
                                }

                                if (stationName.equals("Timisoara") && segmentId >= 7 && segmentId <= 9) {
                                    c3.arriveTrain(trains[numberOfTrains], segmentId);
                                    c3.controlStep();
                                }

                                inputDestination.setText("");
                                inputSegment.setText("");
                                inputStation.setText("");
                                inputTrain.setText("");
                            }
                            else displayStation.append("Invalid line!\n");
                        }
                        else displayStation.append("Invalid destination!\n");
                    }
                    else displayStation.append("Invalid station!\n");
                }
                else displayStation.append("Invalid train!\n");
            }
            else displayStation.append("Maximum number of trains exceeded!\n");

            c1.displayStationState();
            c2.displayStationState();
            c3.displayStationState();
        }
    }
}
