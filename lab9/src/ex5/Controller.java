package ex5;

import java.util.ArrayList;

public class Controller {
    String stationName;
    ArrayList<Controller> neighbourControllers = new ArrayList<Controller>();
    ArrayList<Segment> segments = new ArrayList<Segment>();

    public Controller(String gara) {
        stationName = gara;
    }

    void addNeighbourController(Controller v){
        neighbourControllers.add(v);
    }

    void addControlledSegment(Segment s){
        segments.add(s);
    }

    int getFreeSegmentId() {
        for(Segment s : segments) {
            if(s.hasTrain()==false)
                return s.id;
        }
        return -1;
    }

    void controlStep() {
        for(Segment segment : segments) {
            if(segment.hasTrain()) {
                Train t = segment.getTrain();

                for(Controller neighbour : neighbourControllers)
                    if(t.getDestination().equals(neighbour.stationName)) {
                        int id = neighbour.getFreeSegmentId();

                        if(id == -1) {
                            System.out.println("Train " + t.name + " from " + stationName + " station can't be sent to " +
                                    neighbour.stationName + " station. No line available!");
                            return;
                        }

                        System.out.println("Train " + t.name + " leaves from " + stationName + " station to " +
                                neighbour.stationName + " station.");
                        segment.departTRain();
                        neighbour.arriveTrain(t, id);
                    }
            }
        }
    }

    public void arriveTrain(Train t, int idSegment) {
        for(Segment segment : segments) {
            if(segment.id == idSegment) {
                if (segment.hasTrain() == true) {
                    System.out.println("CRASH! Train " + t.name + " collided with " + segment.getTrain().name +
                            " on line " + segment.id + " in station " + stationName);
                    return;
                }
                else {
                    System.out.println("Train " + t.name + " arrived on line " + segment.id + " in " + stationName + " station.");
                    segment.arriveTrain(t);
                    return;
                }
            }
        }
        System.out.println("Train " + t.name + " cannot be received in " + stationName + " station. Check controller logic algorithm!");
    }


    public void displayStationState() {
        System.out.println("=== STATION " + stationName + " ===");
        for(Segment s : segments) {
            if(s.hasTrain())
                System.out.println("|---------- ID = " + s.id + "\tTrain = " + s.getTrain().name +
                        " to " + s.getTrain().destination + " ----------|");
            else
                System.out.println("|---------- ID = " + s.id + "\tTrain = ______ to ________ ----------|");
        }
    }
}
