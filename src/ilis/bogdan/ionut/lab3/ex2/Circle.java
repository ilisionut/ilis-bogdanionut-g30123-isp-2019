package ilis.bogdan.ionut.lab3.ex2;

public class Circle {
    private double radius;
    private String color;

    public Circle() {
        radius = 1.0;
        color = "red";
    }

    public Circle(  String color){
        this.color = color;
        this.radius = 1.0;

    }

    public Circle( double radius ,String color){
        this.color = color;
        this.radius = radius;

    }


    public double getRadius() {
        return radius;
    }

    public double getArea() {

        return 3.1415*radius*radius ;
    }
}
