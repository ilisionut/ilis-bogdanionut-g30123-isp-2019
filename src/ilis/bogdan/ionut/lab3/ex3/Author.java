package ilis.bogdan.ionut.lab3.ex3;

public class Author {
    private String name;
    private String email;
    private char gender;

    public Author(String name, String email, char gender) {
        if(Character.toLowerCase(gender) == 'm' || Character.toLowerCase(gender) == 'f') {
            this.gender = gender;
        } else System.out.println("Wrong gender");
        this.name = name;
        this.email = email;


    }
    public String getName(){
        return name;
    }
    public String getEmail(){
        return email;
    }

    public char getGender() {
        return gender;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public void AuthorName(){
        System.out.println("Author - "+this.name + " ( " + this.gender + " ) at " + this.email);
    }
}
