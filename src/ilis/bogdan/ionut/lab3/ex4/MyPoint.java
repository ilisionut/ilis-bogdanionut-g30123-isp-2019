package ilis.bogdan.ionut.lab3.ex4;

public class MyPoint {
    private int x;
    private int y;

    public MyPoint() {
        this.x = 0;
        this.y = 0;
    }

    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void SetXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void Coordonates() {
        System.out.println("(" + this.x + "," + this.y + ").");
    }

    public double Distance(int x, int y) {
        return Math.sqrt(Math.pow((x - this.x), 2) + Math.pow((y - this.y), 2));
    }

    public double Distance(MyPoint p1) {
        return Math.sqrt(Math.pow((p1.x - this.x), 2) + Math.pow((p1.y - this.y), 2));
    }


}
