package ilis.bogdan.ionut.lab3.ex4;

public class TestMyPoint {

    public static void main(String[] args) {
        MyPoint p = new MyPoint();
        p.Coordonates();

        MyPoint p1 = new MyPoint(3, 2);
        p1.Coordonates();

        System.out.println(String.valueOf(p.Distance(3, 2)));
        System.out.println(String.valueOf(p1.Distance(3, 3)));
        System.out.println(String.valueOf(p.Distance(p1)));
    }
}