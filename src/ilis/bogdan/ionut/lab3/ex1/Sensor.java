package ilis.bogdan.ionut.lab3.ex1;

public class Sensor {
    private int value;

    Sensor() {
        value = -1;
    }

    public void add(int n) {
        value += n;
    }

    public int get() {
        return value;
    }
}