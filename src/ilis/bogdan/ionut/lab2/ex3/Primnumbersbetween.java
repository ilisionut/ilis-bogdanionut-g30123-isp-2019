package ilis.bogdan.ionut.lab2.ex3;

import java.util.Scanner;

public class Primnumbersbetween {

        public static void main(String[] args) {

            Scanner num = new Scanner(System.in);
            System.out.println("those two numbers are " );
            int A = num.nextInt();
            int B = num.nextInt();
            int count=0;
            for (int i = A; i <= B; i++) {

                boolean flag = false;
                for (int j = 2; j <= i / 2; ++j) {
                    // condition for nonprime number
                    if (i % j == 0) {
                        flag = true;
                        break;
                    }
                }

                if (!flag) {
                    System.out.print(i + ", ");
                    count++;
                }
            }
            System.out.println();
            System.out.println("Count of prime numbers is " + count);

        }
    }

