package ex1;

public class Circle {
    public double radius;
    private String color;


    public Circle() {
        this.color = "red";
        this.radius = 10.0;
    }

    public Circle(double radius, String color) {
        this.radius = radius;
        this.color = color;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return radius * radius * 3.14;
    }
}