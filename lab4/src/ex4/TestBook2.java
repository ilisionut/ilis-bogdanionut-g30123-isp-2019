package ex4;
import java.util.Arrays;
import ex2.Author;

public class TestBook2 {
    public static void main(String[] args) {
        Author[] aut = new Author[3];
        aut[0] = new Author("test1", "test1@gmail.com", 'm');
        aut[1] = new Author("test2", "test2@gmail.com", 'f');
        aut[2] = new Author("test3", "test3@gmail.com", 'm');
        Book2 b = new Book2("carte1", aut, 30, 5);
        Book2 b1 = new Book2("carte2", aut, 40, 10);
        Book2 b2 = new Book2("carte3", aut, 50, 15);
        System.out.println(b.toString() + "\n");
        b.setPrice(50.55);
        b.setQtyInStock(100);
        b.printAuthors();
        b1.setPrice(33.33);
        b1.setQtyInStock(50);
        b1.printAuthors();
        b2.setPrice(40.15);
        b2.setQtyInStock(75);
        b2.printAuthors();
        System.out.println(Arrays.toString(b.getAuthors()) + " " + b.getName() + " " + b.getPrice() + " " + b.getQtyInStock());
        System.out.println(Arrays.toString(b1.getAuthors()) + " " + b1.getName() + " " + b1.getPrice() + " " + b1.getQtyInStock());
        System.out.println(Arrays.toString(b2.getAuthors()) + " " + b2.getName() + " " + b2.getPrice() + " " + b2.getQtyInStock());
    }
}
