package ex6;

public class Test {
    public static void main(String[] args) {
        Shape s = new Shape("yellow", false);
        s.setColor("blue");
        s.setFilled(true);
        System.out.println(s.getColor() + " " + s.isFilled() + s.toString());

        Circle c = new Circle(10, "red", true);
        c.setRadius(2);
        System.out.println(c.getArea() + " " + c.getPerimeter() + " " + c.getRadius() + " " + c.toString());

        Rectangle r = new Rectangle("green", true, 5, 6);
        r.setLenght(2);
        r.setWidth(2);
        System.out.println(r.getWidth() + " " + r.getLenght() + " " + r.getArea() + " " + r.getPerimeter() + " " + r.getArea() + " " + r.toString());

        Square p = new Square("white", true, 4);
        p.setLenght(2);
        System.out.println(p.getSide());
        p.setWidth(2);
        System.out.println(p.getSide());
        p.setSide(2);
        System.out.println(p.getSide());
        System.out.println(p.toString());
    }
}