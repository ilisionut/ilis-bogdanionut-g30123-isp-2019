package ex6;

public class Circle extends Shape {
    private double radius;
    private String color;
    private static final float PI = 3.1415f;

    public double Circle() {
        this.radius = 1.0;
        this.color = "red";
    }

    public double Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return PI * radius * radius;
    }

    public String toString() {
        System.out.println("A shape with color" + super.color + " and" + super.filled);
        return null;
    }
}
