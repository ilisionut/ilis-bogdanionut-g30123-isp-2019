package ex3;


import ex2.Author;

public class Book {
    private String name;
    private double price;
    private int gtyInStock;
    private Author author;


    public Book(String name, double price, int gtyInStock,Author author) {
        this.name = name;
        this.price = price;
        this.gtyInStock = 0;
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getGtyInStock() {
        return gtyInStock;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setGtyInStock(int gtyInStock) {
        this.gtyInStock = gtyInStock;
    }

    @Override
    public String toString() {
        return "Book -" + this.name + this.author.toString();
    }
}

