package ex1;
import java.util.Observable;

public class Sensor extends Observable implements Runnable {
    static int MAX_VALUE = 70;
    static int MIN_VALUE = -30;

    Thread thread;

    public void start() {
        if(thread == null) {
            thread = new Thread(this);
            thread.start();
        }
    }

    public void run() {

    }
}
